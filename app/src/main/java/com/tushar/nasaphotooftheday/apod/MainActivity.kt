package com.tushar.nasaphotooftheday.apod

import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.youtube.player.*
import com.google.android.youtube.player.YouTubeThumbnailLoader.OnThumbnailLoadedListener
import com.tushar.nasaphotooftheday.R
import com.tushar.nasaphotooftheday.apod.api.ApiClient
import com.tushar.nasaphotooftheday.apod.api.NpodNetworkInterface
import com.tushar.nasaphotooftheday.apod.data.NpodData
import com.tushar.nasaphotooftheday.apod.receiver.NetworkInterface
import com.tushar.nasaphotooftheday.apod.receiver.NetworkStateReceiver
import com.tushar.nasaphotooftheday.apod.repository.NetworkState
import com.tushar.nasaphotooftheday.apod.repository.NpodRepository
import com.tushar.nasaphotooftheday.apod.utils.Utils.Utility
import com.tushar.nasaphotooftheday.apod.utils.ViewUtils.ViewUtil
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), YouTubePlayer.OnInitializedListener,
    View.OnClickListener, YouTubePlayer.PlaybackEventListener,
    YouTubeThumbnailView.OnInitializedListener {

    @BindView(R.id.tv_title)
    lateinit var tvTitle: TextView

    @BindView(R.id.iv_calendar)
    lateinit var ivCalendar: ImageView

    @BindView(R.id.iv_zoom)
    lateinit var ivZoom: ImageView

    @BindView(R.id.tv_description)
    lateinit var tvDescription: TextView

    @BindView(R.id.iv_expand)
    lateinit var ivExpand: ImageView

    private lateinit var viewModel: NpodViewModel
    private lateinit var apodRepository: NpodRepository
    private var isImage: Boolean = true

    @BindView(R.id.iv_apod)
    lateinit var ivApod: ImageView

    @BindView(R.id.layout_top)
    lateinit var layoutTop: ConstraintLayout

    @BindView(R.id.cv_title)
    lateinit var cardViewTitle: CardView

    @BindView(R.id.cv_description)
    lateinit var cardViewDescription: CardView

    @BindView(R.id.youtube_thumbnail)
    lateinit var youtubeThumbnail: YouTubeThumbnailView


    @BindView(R.id.iv_fab)
    lateinit var ivFab: ImageView


    var VIDEO_ID: String? = null
    var day = 0
    var month = 0;
    var year = 2020
    var calendar: Calendar? = null
    var date: Date? = null
    var networkStateReceiver: NetworkStateReceiver? = null
    private val TAG = "MainActivity"

    var animationDisplay: Animation? = null
    var animationFade: Animation? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Initialize references
        initReferences()

    }


    fun initReferences() {
        //Initializing reference
        ButterKnife.bind(this)
//        tvDescription.setMovementMethod(ScrollingMovementMethod())
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Adding clickListener to view
        ivZoom.setOnClickListener(this)
        ivCalendar.setOnClickListener(this)
        calendar = Calendar.getInstance()
        ivExpand.setOnClickListener(this)
        date = Date()


        //initializing network state receiver
        networkStateReceiver = NetworkStateReceiver(object : NetworkInterface {
            override fun connected() {
                Utility.dismissNetworkDialog()
            }

            override fun disconnected() {
                Utility.displayNoNetworkDialog(this@MainActivity)

            }
        })


        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkStateReceiver, intentFilter)


        if (Utility.isNetworkAvailable(this)) {
            val apiService: NpodNetworkInterface = ApiClient.getClient()
            apodRepository = NpodRepository(apiService)
            viewModel = getViewModel()
            viewModel.npodDetails.observe(this, androidx.lifecycle.Observer { bindUI(it) })
            viewModel.networkState.observe(this, androidx.lifecycle.Observer {


                if (it.status.equals(NetworkState.ERROR)) {
                    Utility.dismissLoading()
                    Toast.makeText(
                        this,
                        getString(R.string.went_wrong), Toast.LENGTH_SHORT
                    ).show()
                }

            })
            Utility.displayLoadingDialog(this)

        } else {
            Utility.displayNoNetworkDialog(this)
        }

        animationDisplay = AnimationUtils.loadAnimation(this, R.anim.slide_in_pop_up)
        animationFade = AnimationUtils.loadAnimation(this, R.anim.slide_out_pop_up)
    }


    fun bindUI(it: NpodData) {
        //Updating UI
        Utility.dismissLoading()
        tvDescription!!.movementMethod = ScrollingMovementMethod()
        tvTitle.text = it.title
        tvDescription.text = it.explanation

        //loading image to ImageView if mediaType is image
        if (it.mediaType.equals("image")) {
            Utility.updateAnalytics(true)
            ivZoom.setImageDrawable(getDrawable(R.drawable.zoom_in))
            isImage = true
            ViewUtil.setViewVisible(ivApod)
            ViewUtil.setViewGone(youtubeThumbnail)
            Glide.with(this)
                .load(it.url)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivApod)
        } else {
            Utility.updateAnalytics(false)
            //Handling video
            ivZoom.setImageDrawable(getDrawable(R.drawable.video))
            isImage = false
            VIDEO_ID = Utility.extractYTId(it.url)
//            layoutTop.visibility = View.GONE
//            videoView.initialize(API_KEY, this)
//            ivZoom.visibility = View.GONE

            loadThumbnail(VIDEO_ID!!)
            ViewUtil.setViewGone(ivApod)
            ViewUtil.setViewVisible(youtubeThumbnail)
        }
    }

    fun getViewModel(): NpodViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return NpodViewModel(apodRepository) as T
            }
        })[NpodViewModel::class.java]
    }

    fun showDatePicker() {

        var alertDialog: AlertDialog?
        var view: View = layoutInflater.inflate(R.layout.date_picker_layout, null, false)
        var alertBuilder: AlertDialog.Builder?
        var datePicker: DatePicker = view.findViewById(R.id.calendar)
        calendar!!.set(Calendar.MONTH, 5)
        calendar!!.set(Calendar.DAY_OF_MONTH, 16)
        calendar!!.set(Calendar.YEAR, 1995)
        datePicker.maxDate = date!!.time
        datePicker.minDate = calendar!!.timeInMillis

        var btnConfirm = view.findViewById<Button>(R.id.btn_confirm)
        var btnCancel = view.findViewById<Button>(R.id.btn_cancel)
        alertBuilder =
            AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        alertDialog = alertBuilder.create()
        alertBuilder.setCancelable(true)
        alertDialog.setView(view)
        day = datePicker.dayOfMonth
        month = datePicker.month
        year = datePicker.year

        btnCancel.setOnClickListener { alertDialog.dismiss() }
        btnConfirm.setOnClickListener {
            alertDialog.dismiss()
            day = datePicker.dayOfMonth
            month = datePicker.month
            this.year = datePicker.year

            Log.d("sd", "Choosen is $day -  $month - $year")
            //Increaging month by 1
            month++;
            var date: String = year.toString() + "-" + month.toString() + "-" + day.toString()
//            viewModelByDate = null
//            viewModelByDate = getByDateViewModel(date)
//            viewModelByDate!!.npodDetails.observe(this, androidx.lifecycle.Observer { bindUI(it) })
            viewModel!!.getByDate(date).observe(this, androidx.lifecycle.Observer { bindUI(it) })
            Utility.displayLoadingDialog(this)

        }
        alertDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        //Unregister Network State Receiver
        unregisterReceiver(networkStateReceiver)
    }

    var youTubePlayerFragment: YouTubePlayerFragment? = null
    fun loadThumbnail(videoId: String) {
        ivZoom.setImageDrawable(getDrawable(R.drawable.video))

        youTubePlayerFragment = fragmentManager
            .findFragmentById(R.id.youtube_player_fragment) as YouTubePlayerFragment

        youTubePlayerFragment!!.initialize(Utility.API_KEY, this)

        youtube_thumbnail.initialize(
            Utility.API_KEY,
            object : YouTubeThumbnailView.OnInitializedListener {
                override fun onInitializationSuccess(
                    youTubeThumbnailView: YouTubeThumbnailView,
                    youTubeThumbnailLoader: YouTubeThumbnailLoader
                ) {
                    youTubeThumbnailLoader.setVideo(videoId)
                    youTubeThumbnailLoader.setOnThumbnailLoadedListener(object :
                        OnThumbnailLoadedListener {
                        override fun onThumbnailLoaded(
                            youTubeThumbnailView: YouTubeThumbnailView,
                            s: String
                        ) {
                            youTubeThumbnailLoader.release()
                        }

                        override fun onThumbnailError(
                            youTubeThumbnailView: YouTubeThumbnailView,
                            errorReason: YouTubeThumbnailLoader.ErrorReason
                        ) {
                        }
                    })
                }

                override fun onInitializationFailure(
                    youTubeThumbnailView: YouTubeThumbnailView,
                    youTubeInitializationResult: YouTubeInitializationResult
                ) {
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.went_wrong),
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })


    }

    var youtubePlayer: YouTubePlayer? = null

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
//        TODO("Not yet implemented")
        p1!!.setFullscreenControlFlags(
            YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION or
                    YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE
        )


        if (!p2) {
            youtubePlayer = p1
//            p1.loadVideo(VIDEO_ID)
//            p1.pause()
//            p1.setPlaybackEventListener(this)
        }
//        p1.pause()
    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
        Toast.makeText(this@MainActivity, getString(R.string.went_wrong), Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        //Handling view Clicks
        when (v) {
            //when click is on Calendar
            ivCalendar -> {
                //show date picker Calendar
                showDatePicker()
            }
            ivZoom -> {
                //handling zoom click
                zoomClicked()
            }
            ivExpand -> {
                handleExpand()
            }
        }

    }


    fun zoomClicked() {


        //hiding views
        Log.d(TAG, "zoomClicked: " + "Is Image " + isImage)
        if (isImage) {
            if (ivCalendar.isVisible) {
                ivCalendar.animation = animationFade
                cardViewTitle.animation = animationFade
                cardViewDescription.animation = animationFade
                ViewUtil.setViewInvisible(ivCalendar, cardViewTitle, cardViewDescription)
                ivZoom.setImageDrawable(getDrawable(R.drawable.cancel))
            } else {
                ivCalendar.animation = animationDisplay
                cardViewTitle.animation = animationDisplay
                cardViewDescription.animation = animationDisplay
                ViewUtil.setViewVisible(ivCalendar, cardViewTitle, cardViewDescription)
                ivZoom.setImageDrawable(getDrawable(R.drawable.zoom_in))

            }

        } else {
            try {
                if (!youtubePlayer!!.isPlaying) {

                    layoutTop.animation = animationFade
                    ivZoom.animation = animationFade
                    youtubeThumbnail.animation = animationFade
                    youtubePlayer!!.loadVideo(VIDEO_ID)
//                    p1.pause()
                    youtubePlayer!!.setPlaybackEventListener(this)


                    ViewUtil.setViewGone(layoutTop, ivZoom, youtube_thumbnail)
                    youtubePlayer!!.play()
                }
            } catch (e: Exception) {
            }
        }
    }

    override fun onSeekTo(p0: Int) {
    }

    override fun onBuffering(p0: Boolean) {
    }

    override fun onPlaying() {
        ivFab.visibility = View.GONE
    }

    override fun onStopped() {
    }

    override fun onPaused() {
        ivFab!!.visibility = View.VISIBLE
        ivFab.setOnClickListener {
            layoutTop.animation = animationDisplay
            ivZoom.animation = animationDisplay
            youtubeThumbnail.animation = animationDisplay
            ivFab.animation = animationFade
            ViewUtil.setViewVisible(layoutTop, youtubeThumbnail, ivZoom)
            ViewUtil.setViewGone(ivFab)
        }
    }

    override fun onInitializationSuccess(p0: YouTubeThumbnailView?, p1: YouTubeThumbnailLoader?) {
        Log.d("Thumbnail", "went right")
    }

    override fun onInitializationFailure(
        p0: YouTubeThumbnailView?,
        p1: YouTubeInitializationResult?
    ) {
        Log.d("Thumbnail", "went wrong")
    }

    fun handleExpand() {
//        tvDescription.setExpandInterpolator(OvershootInterpolator())
//        tvDescription.setCollapseInterpolator(OvershootInterpolator())
//        tvDescription.toggle()

    }
}
