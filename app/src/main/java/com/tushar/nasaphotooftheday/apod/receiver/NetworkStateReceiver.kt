package com.tushar.nasaphotooftheday.apod.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

class NetworkStateReceiver(broadcastInterface: NetworkInterface) :
    BroadcastReceiver() {
    var broadcastInterface: NetworkInterface
    override fun onReceive(context: Context, intent: Intent) {
        var noConnectivity = false
        if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
            if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                noConnectivity = true
                broadcastInterface.disconnected()
            }
        }
        if (!noConnectivity) {
            broadcastInterface.connected()
        }
    }
    init {
        this.broadcastInterface = broadcastInterface
    }
}