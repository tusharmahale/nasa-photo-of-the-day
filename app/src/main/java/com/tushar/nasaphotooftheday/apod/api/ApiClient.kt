package com.tushar.nasaphotooftheday.apod.api

import com.tushar.nasaphotooftheday.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://api.nasa.gov/planetary/"

object ApiClient {
    fun getClient(): NpodNetworkInterface {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) logging.setLevel(HttpLoggingInterceptor.Level.BODY) else logging.setLevel(
            HttpLoggingInterceptor.Level.NONE
        )


        var requestInterceptor = Interceptor { chain ->

            val url = chain.request()
                .url()
                .newBuilder()
                .build()
            val request: Request = chain.request()
                .newBuilder()
                .url(url)
                .build()

            return@Interceptor chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(logging)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NpodNetworkInterface::class.java)
    }
}