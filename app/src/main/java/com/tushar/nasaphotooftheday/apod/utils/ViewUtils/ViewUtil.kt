package com.tushar.nasaphotooftheday.apod.utils.ViewUtils

import android.view.View

class ViewUtil {
    companion object {
        fun setViewInvisible(vararg views: View) {
            for (v in views) v.visibility = View.GONE
        }

        fun setViewVisible(vararg views: View) {
            for (v in views) v.visibility = View.VISIBLE
        }
        fun setViewGone(vararg views: View) {
            for (v in views) v.visibility = View.GONE
        }

    }
}