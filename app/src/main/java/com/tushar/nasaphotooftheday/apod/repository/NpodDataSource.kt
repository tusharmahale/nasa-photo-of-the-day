package com.tushar.nasaphotooftheday.apod.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tushar.nasaphotooftheday.apod.api.NpodNetworkInterface
import com.tushar.nasaphotooftheday.apod.data.NpodData
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class NpodDataSource (private  val apiService:NpodNetworkInterface, private val  compositeDisposable:CompositeDisposable){

private val _networkState = MutableLiveData<NetworkState>()
    val networkState:LiveData<NetworkState>
    get() = _networkState

    private val _apodResponse = MutableLiveData<NpodData>()
    val apodResponse:LiveData<NpodData>
        get() = _apodResponse


    fun fetchNpodNetworkInterface(){
        _networkState.postValue(NetworkState.LOADING)

        try{
            compositeDisposable.add(apiService.getNpodDetails()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    _apodResponse.postValue(it)
                    _networkState.postValue(NetworkState.LOADED)
                },{
                    _networkState.postValue(NetworkState.ERROR)

                Log.e("Error","Something not right")}))
    }
    catch (e:Exception){
        Log.e("Error","Something not right ${e.message}")
    }
    }
    fun fetchApodNetworkInterface(date:String){
        _networkState.postValue(NetworkState.LOADING)

        try{
            compositeDisposable.add(apiService.getNpodByDate(date)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    _apodResponse.postValue(it)
                    _networkState.postValue(NetworkState.LOADED)
                },{
                    _networkState.postValue(NetworkState.ERROR)

                    Log.e("Error","Something not right "+it )}))
        }
        catch (e:Exception){
            Log.e("Error","Something not right ${e.message}")
        }
    }



}