package com.tushar.nasaphotooftheday.apod.receiver

interface NetworkInterface {
    fun connected()
    fun disconnected()

}