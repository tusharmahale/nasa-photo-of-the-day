package com.tushar.nasaphotooftheday.apod

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tushar.nasaphotooftheday.apod.data.NpodData
import com.tushar.nasaphotooftheday.apod.repository.NetworkState
import com.tushar.nasaphotooftheday.apod.repository.NpodRepository
import io.reactivex.disposables.CompositeDisposable


class NpodViewModel(private val apodRepository: NpodRepository) : ViewModel() {


    private val compositeDisposable = CompositeDisposable()

    val networkState: LiveData<NetworkState> by lazy {
        apodRepository.getApodNetworkState()
    }

    val npodDetails: LiveData<NpodData> by lazy {
        apodRepository.fetchingApodData(compositeDisposable)
    }

    fun getByDate(date:String): LiveData<NpodData> {
        return apodRepository.fetchingNpodData(compositeDisposable,date)

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}