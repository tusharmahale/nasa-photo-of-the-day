package com.tushar.nasaphotooftheday.apod.repository

import androidx.lifecycle.LiveData
import com.tushar.nasaphotooftheday.apod.api.NpodNetworkInterface
import com.tushar.nasaphotooftheday.apod.data.NpodData
import io.reactivex.disposables.CompositeDisposable

class NpodRepository(private val apiService: NpodNetworkInterface) {
    lateinit var apodDataSource: NpodDataSource

    fun fetchingApodData(compositeDisposable: CompositeDisposable): LiveData<NpodData> {
        apodDataSource = NpodDataSource(apiService, compositeDisposable)
        apodDataSource.fetchNpodNetworkInterface()
        return apodDataSource.apodResponse
    }

    fun getApodNetworkState(): LiveData<NetworkState> {
        return apodDataSource.networkState
    }

    fun fetchingNpodData(compositeDisposable: CompositeDisposable,date:String): LiveData<NpodData> {
        apodDataSource = NpodDataSource(apiService, compositeDisposable)
        apodDataSource.fetchApodNetworkInterface(date)
        return apodDataSource.apodResponse
    }

}