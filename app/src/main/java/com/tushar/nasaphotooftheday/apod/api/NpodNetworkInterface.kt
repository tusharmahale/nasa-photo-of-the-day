package com.tushar.nasaphotooftheday.apod.api

import com.tushar.nasaphotooftheday.apod.data.NpodData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NpodNetworkInterface {
    //https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=2019-10-21
//    planetary/apod?api_key=DEMO_KEY
    @GET("apod?api_key=DEMO_KEY")
    fun getNpodDetails(): Single<NpodData>

    @GET("apod?api_key=DEMO_KEY")
    fun getNpodByDate(@Query("date") d:String) : Single<NpodData>


//@GET("planetary/apod?api_key=DEMO_KEY")
}