package com.tushar.nasaphotooftheday.apod.utils.Utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import com.airbnb.lottie.LottieAnimationView
import com.google.firebase.database.*
import com.tushar.nasaphotooftheday.R
import java.util.regex.Matcher
import java.util.regex.Pattern


class Utility {
    companion object {
        var alertDialogLoading: AlertDialog? = null
        var alertDialogNetwork: AlertDialog? = null
        var API_KEY = "AIzaSyDsiYlJDMY7Qx27nVI6Db5ZR1B-VTPQaXc"

        //Display loading dialog
        fun displayLoadingDialog(activity: Activity) {

            var view: View = activity.layoutInflater.inflate(R.layout.lottie_dialog, null, false)
            var alertBuilder: AlertDialog.Builder?
            var lottieAnimationView: LottieAnimationView = view.findViewById(R.id.lottie_view)
            lottieAnimationView.setAnimation("loading.json")
            alertBuilder = AlertDialog.Builder(
                activity,
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar
            )
            alertDialogLoading = alertBuilder.create()
            alertBuilder!!.setCancelable(false)
            alertDialogLoading!!.setView(view)
            alertDialogLoading!!.show()
            Log.d("TAG", "displayLoadingDialog: ")
        }

        fun dismissLoading() {
            try {
                Log.d("TAG", "dismissLoading: ")


                if (alertDialogLoading != null)
                    alertDialogLoading!!.dismiss()
            } catch (e: java.lang.Exception) {
            }
        }


        fun displayNoNetworkDialog(activity: Activity) {

            var view: View = activity.layoutInflater.inflate(R.layout.lottie_dialog, null)
            var alertBuilder: AlertDialog.Builder?
            var lottieAnimationView: LottieAnimationView = view.findViewById(R.id.lottie_view)
            var retryButton = view.findViewById<Button>(R.id.btn_retry)
//            retryButton.setOnClickListener(clickListener)
            retryButton.visibility = View.VISIBLE
            retryButton.setOnClickListener {
                Toast.makeText(
                    activity,
                    activity.getString(R.string.retrying_network),
                    Toast.LENGTH_SHORT
                ).show()
                dismissNetworkDialog()

                if (isNetworkAvailable(activity)) {
                    dismissNetworkDialog()
                }
            }
            lottieAnimationView.setAnimation("no_internet.json")
            alertBuilder = AlertDialog.Builder(
                activity,
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar
            )
            alertDialogNetwork = alertBuilder.create()
            alertBuilder!!.setCancelable(true)
//            alertDialogNetwork!!.setCanceledOnTouchOutside(false)
//            alertDialogNetwork!!.setCancelable(false)
            alertDialogNetwork!!.setView(view)
            alertDialogNetwork!!.show()

        }


        fun dismissNetworkDialog() {
            try {
                alertDialogNetwork!!.dismiss()

            } catch (e: Exception) {
                Log.d("Dismiss", "Exception occurred " + e.message)

            }
            Log.d("Dismiss", "trying to dismiss")

        }


        fun extractYTId(url: String?): String? {
            var videoId: String? = null
            val regex =
                "http(?:s)?:\\/\\/(?:m.)?(?:www\\.)?youtu(?:\\.be\\/|be\\.com\\/(?:watch\\?(?:feature=youtu.be\\&)?v=|v\\/|embed\\/|user\\/(?:[\\w#]+\\/)+))([^&#?\\n]+)"
            val pattern =
                Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
            val matcher: Matcher = pattern.matcher(url)
            if (matcher.find()) {
                videoId = matcher.group(1)
            }
            return videoId
        }

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var activeNetworkInfo: NetworkInfo? = null
            if (connectivityManager != null) {
                activeNetworkInfo = connectivityManager.activeNetworkInfo
            }
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }


        //Updating analytics to firebase it's image or not
        fun updateAnalytics(isImage: Boolean) {
            var viewCount: Long = 0

            //FirebaseDatabase instance
            val database: FirebaseDatabase = FirebaseDatabase.getInstance()

            //Creating database Reference
            var databaseReference: DatabaseReference

            //Creating references to image
            if (isImage) {
                databaseReference = database.getReference("npod_views/images")
            } else {
                //Creating references to video
                databaseReference = database.getReference("npod_views/video")
            }
            databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.value == null) {
                        viewCount = 0
                    } else viewCount = dataSnapshot.value as Long

                    //Increasing and setting viewCount by 1
                    databaseReference.setValue(viewCount + 1)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })

        }
    }
}